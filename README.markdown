# React/Redux Blog

By: Baskoro Indrayana <<baskoroindrayana@outlook.com>>

## Description

This is a CRUD blog app I built, while following [this tutorial](https://medium.com/@rajaraodv/a-guide-for-building-a-react-redux-crud-app-7fe0b8943d0f#.bk9ewllu4). I thought about following along as I want to build my own blog in the future.

I primarily used React/Redux and Node.js in making this blog.

## TODO (if any)
